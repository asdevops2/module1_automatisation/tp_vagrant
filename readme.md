## TP VAGRANT INIT 

Vous devez créer 1 VM 'lead' et 3 VMs 'node' 1 à 3.
En terme de ressources :
le master avec 2 CPU et 1024 Mb et un volume de 4Gb pour le stockage
les nodes avec 1 CPU et 512 Mb et un volume de 4Gb pour le stockage
Pour chaque VM (master inclus) vous devez créer un volume (sync folder) partagé avec un
répertoire de votre poste portant le nom du node et un répertoire html qui contient un fichier
index.html avec dans l'en-tête le nom de l'hôte :

	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8">
		<title>Hello World Vagrant</title>
	</head>
	<body>
		<h1>This is the server Node 3 </h1>
	</body>
	</html>

/vm/master/html/index.html
/vm/node1/html/index.html
/vm/node2/html/index.html
/vm/node3/html/index.html
